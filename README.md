1. npm install
2. app/ tempat untuk developing
	- run local gulp serve
	- untuk live editing hanya ada di gulp serve
4. dist/ tempat untuk production
	- run gulp serve:dist
	- Jadi Backend akan dapat file yang di folder dist
5. app/assets/script/main.js
	- untuk tempat menaruh global js
	- saat di compile dist akan otomatis terminify
6. app/assets/style/ 
	- tempat untuk menaruh style 
	- jika ingin menambah file beri type file dengan scss maka otomatis akan saat di compile akan jadi 1 file dan terminify
7. vendor (tempat untuk menaruh vendor/plugin seperti jquery, dll)
	- script (tempat vendor/plugin js saja)
	- styles (tempat vendor/plugin css saja)
	- untuk yg ini saat menambah file juga perlu di include di foot.html
	- saat compile dist vedor akan otomatis jadi 1 file dan terminify